// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Rules/Controllers/NodeSystemPlayerController.h>

#include "StaticTheaterPlayerController.generated.h"

/**
 *
 */
UCLASS()
class STATICTHEATER_API AStaticTheaterPlayerController
	: public ANodeSystemPlayerController {

	GENERATED_BODY()

public:
	AStaticTheaterPlayerController(const FObjectInitializer& ObjectInitializer);
};
