// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <UObject/Interface.h>

#include "StaticTheaterPawnInterface.generated.h"

class INodeSystemPathInterface;

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UStaticTheaterPawnInterface : public UInterface {
	GENERATED_BODY()
};

/**
 *
 */
class STATICTHEATER_API IStaticTheaterPawnInterface {
	GENERATED_BODY()

public:
	/**  */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|Pawn",
		Meta = (Keywords = "6b6ae4 Static Theater Pawn Follow Path"))
	void FollowPath(const TScriptInterface<INodeSystemPathInterface>& Value);

protected:
	virtual void FollowPath_Implementation(
		const TScriptInterface<INodeSystemPathInterface>& Value) = 0;
};
