// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterResourceKeyInterface.h"

#include <Engine/DataAsset.h>

#include "StaticTheaterResourceKeyBase.generated.h"

/**
 *
 */
UCLASS(Abstract, BlueprintType, Blueprintable, Meta = (ShowWorldContextPin))
class STATICTHEATER_API UStaticTheaterResourceKeyBase
	: public UDataAsset
	, public IStaticTheaterResourceKeyInterface {

	GENERATED_BODY()

protected:
	UE_NODISCARD UObject* NewResourceForPlayerController_Unsafe_Implementation(
		APlayerController* const PlayerController) const override {

		unimplemented();
		return nullptr;
	}
	void DeleteResource_Unsafe_Implementation(
		UObject* const Value) const override {

		unimplemented();
	}
};
