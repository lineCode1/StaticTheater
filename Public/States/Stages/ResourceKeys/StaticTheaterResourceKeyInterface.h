// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <UObject/Interface.h>

#include "StaticTheaterResourceKeyInterface.generated.h"

class APlayerController;

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UStaticTheaterResourceKeyInterface : public UInterface {
	GENERATED_BODY()
};

/**
 * Implements the creation and destruction of resources. And also used to access
 * them.
 */
class STATICTHEATER_API IStaticTheaterResourceKeyInterface {
	GENERATED_BODY()

public:
	/**
	 *
	 *
	 * @warning Check Data && PlayerController!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|ResourceKey",
		Meta = (
			// clang-format off
			Keywords =
				"6b6ae4 Static Theater Resource Key New For Player Controller Unsafe",
			// clang-format on
			DisplayName = NewResourceFor_Unsafe))
	UObject* NewResourceForPlayerController_Unsafe(
		APlayerController* PlayerController) const;
	/**
	 *
	 *
	 * @warning Check Value!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|ResourceKey",
		Meta = (
			// clang-format off
			Keywords = "6b6ae4 Static Theater Resource Key Delete Unsafe"
			// clang-format on
			))
	void DeleteResource_Unsafe(UObject* Value) const;

protected:
	UE_NODISCARD virtual UObject*
	NewResourceForPlayerController_Unsafe_Implementation(
		APlayerController* const PlayerController) const = 0;
	virtual void DeleteResource_Unsafe_Implementation(
		UObject* const Value) const = 0;
};
