// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterWidgetResourceKey.h"

#include "StaticTheaterTextWidgetResourceKey.generated.h"

/**
 *
 */
UCLASS()
class STATICTHEATER_API UStaticTheaterTextWidgetResourceKey
	: public UStaticTheaterWidgetResourceKey {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ResourceKey)
	TSubclassOf<class UStaticTheaterTextWidgetBase> WidgetClass;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetWidgetClass() const noexcept {
		return WidgetClass;
	}

protected:
	UE_NODISCARD UObject* NewResourceForPlayerController_Unsafe_Implementation(
		APlayerController* const PlayerController) const;
};
