// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterResourceKeyBase.h"

#include "StaticTheaterWidgetResourceKey.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterWidgetResourceKey
	: public UStaticTheaterResourceKeyBase {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ResourceKey)
	int32 WidgetZOrder;

public:
	/**  */
	UE_NODISCARD FORCEINLINE auto GetWidgetZOrder() const noexcept {
		return WidgetZOrder;
	}

protected:
	void DeleteResource_Unsafe_Implementation(
		UObject* const Value) const override;
};
