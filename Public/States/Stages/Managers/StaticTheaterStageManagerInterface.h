// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <UObject/Interface.h>

#include "StaticTheaterStageManagerInterface.generated.h"

class IStaticTheaterResourceKeyInterface;

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UStaticTheaterStageManagerInterface : public UInterface {
	GENERATED_BODY()
};

/**
 *
 */
class STATICTHEATER_API IStaticTheaterStageManagerInterface {
	GENERATED_BODY()

public:
	/**
	 *
	 *
	 * @warning Key!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|StageManager",
		Meta = (
			// clang-format off
			Keywords = "6b6ae4 Static Theater Stage Get Resource By Key Unsafe",
			// clang-format on
			DisplayName = GetResourceBy_Unsafe))
	UObject* GetResourceByResourceKey_Unsafe(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key);
	/**
	 *
	 *
	 * @warning Key!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|StageManager",
		Meta = (
			// clang-format off
			Keywords =
				"6b6ae4 Static Theater Stage Remove Resource By Key Unsafe",
			// clang-format on
			DisplayName = RemoveResourceBy_Unsafe))
	void RemoveResourceByResourceKey_Unsafe(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key);

	/**  */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|StageManager",
		Meta = (Keywords = "6b6ae4 Static Theater Stage Empty Resources"))
	void EmptyResources();

protected:
	UE_NODISCARD virtual UObject*
	GetResourceByResourceKey_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key) = 0;
	virtual void RemoveResourceByResourceKey_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key) = 0;

	virtual void EmptyResources_Implementation() = 0;
};
