// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <States/Snapshots/NodeSystemSnapshot.h>

#include "StaticTheaterSnapshot.generated.h"

/**
 *
 */
UCLASS()
class STATICTHEATER_API UStaticTheaterSnapshot : public UNodeSystemSnapshot {
	GENERATED_BODY()
};
