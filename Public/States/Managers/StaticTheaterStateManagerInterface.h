// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <UObject/Interface.h>

#include "StaticTheaterStateManagerInterface.generated.h"

class IStaticTheaterStageManagerInterface;

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UStaticTheaterStateManagerInterface : public UInterface {
	GENERATED_BODY()
};

/**
 *
 */
class STATICTHEATER_API IStaticTheaterStateManagerInterface {
	GENERATED_BODY()

public:
	/**  */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|StateManager",
		Meta = (Keywords = "6b6ae4 Static Theater State Manager Get Stage"))
	TScriptInterface<IStaticTheaterStageManagerInterface> GetStageManager();

protected:
	UE_NODISCARD virtual TScriptInterface<IStaticTheaterStageManagerInterface>
	GetStageManager_Implementation() = 0;
};
