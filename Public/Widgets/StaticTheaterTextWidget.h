// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterTextWidgetBase.h"

#include "StaticTheaterTextWidget.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterTextWidget
	: public UStaticTheaterTextWidgetBase {

	GENERATED_BODY()

private:
	/**  */
	UPROPERTY(BlueprintReadOnly,
		Category = TextWidget,
		Meta = (BindWidget, BlueprintProtected, AllowPrivateAccess = true))
	TObjectPtr<class URichTextBlock> RichTextBlock;

protected:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetRichTextBlock() const noexcept {
		return RichTextBlock;
	}

protected:
	virtual void SetText_Implementation(const FText& Value);
	virtual void SetTextStyleSet_Unsafe_Implementation(UDataTable* const Value);
};
