// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Blueprint/UserWidget.h>

#include "StaticTheaterTextWidgetBase.generated.h"

class UDataTable;

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterTextWidgetBase : public UUserWidget {
	GENERATED_BODY()

public:
	/**  */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|TextWidget",
		Meta = (Keywords = "6b6ae4 Static Theater Text Widget Set"))
	void SetText(const FText& Value);
	/**
	 *
	 *
	 * @warning Check Value!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|TextWidget",
		Meta = (
			// clang-format off
			Keywords = "6b6ae4 Static Theater Text Widget Set Style Set Unsafe"
			// clang-format on
			))
	void SetTextStyleSet_Unsafe(UDataTable* Value);

protected:
	virtual void SetText_Implementation(const FText& Value) { unimplemented(); }
	virtual void SetTextStyleSet_Unsafe_Implementation(
		UDataTable* const Value) {
		unimplemented();
	}
};
