// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Widgets/NodeSystemNodeWidget.h>

#include "StaticTheaterNodeWidget.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterNodeWidget
	: public UNodeSystemNodeWidget {

	GENERATED_BODY()
};
