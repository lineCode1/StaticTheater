// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Widgets/NodeSystemPathWidget.h>

#include "StaticTheaterPathWidget.generated.h"

/**
 *
 */
UCLASS()
class STATICTHEATER_API UStaticTheaterPathWidget
	: public UNodeSystemPathWidget {

	GENERATED_BODY()
};
