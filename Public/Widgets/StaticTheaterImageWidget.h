// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Blueprint/UserWidget.h>

#include "StaticTheaterImageWidget.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterImageWidget : public UUserWidget {
	GENERATED_BODY()

private:
	/**  */
	UPROPERTY(BlueprintReadOnly,
		Category = DialogWidget,
		Meta = (BindWidget, AllowPrivateAccess = true))
	TObjectPtr<class UScaleBox> ImageScaleBox;
	/**  */
	UPROPERTY(BlueprintReadOnly,
		Category = DialogWidget,
		Meta = (BindWidget, AllowPrivateAccess = true))
	TObjectPtr<class UImage> Image;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetImageScaleBox() const noexcept {
		return ImageScaleBox;
	}
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetImage() const noexcept {
		return Image;
	}
};
