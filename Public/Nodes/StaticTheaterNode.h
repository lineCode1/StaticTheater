// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Nodes/NodeSystemNode.h>

#include "StaticTheaterNode.generated.h"

/**
 *
 */
UCLASS()
class STATICTHEATER_API UStaticTheaterNode : public UNodeSystemNode {
	GENERATED_BODY()
	MModuleCoreGeneratedBodyUE(UStaticTheaterNode);

protected:
#if WITH_EDITORONLY_DATA
	/**
	 *
	 *
	 * @warning EditorOnly!
	 *
	 * @note Workarounds the UE issue(Only descendants of an actor can be set to
	 * an interface variable in blueprints).
	 */
	UPROPERTY(EditDefaultsOnly,
		Instanced,
		Category = Node,
		Meta = (DisplayAfter = Operation))
	TArray<TObjectPtr<class UStaticTheaterActionBase>> Editor_Actions;
#endif
	/**  */
	UPROPERTY()
	TArray<TScriptInterface<class IStaticTheaterActionInterface>> Actions;

public:
#if WITH_EDITORONLY_DATA
	/**
	 *
	 *
	 * @warning EditorOnly!
	 */
	UE_NODISCARD FORCEINLINE const auto& GetEditor_Actions() const noexcept {
		return Editor_Actions;
	}
#endif
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetActions() const noexcept {
		return Actions;
	}

protected:
#if WITH_EDITORONLY_DATA
	/**
	 *
	 *
	 * @warning EditorOnly!
	 */
	void OnSetEditor_Actions();
#endif

public:
#if WITH_EDITOR
	void PostEditChangeChainProperty(
		FPropertyChangedChainEvent& Value) override;
#endif
};
