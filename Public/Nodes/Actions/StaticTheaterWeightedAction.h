// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <CoreMinimal.h>

#include "StaticTheaterWeightedAction.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct STATICTHEATER_API FStaticTheaterWeightedAction {
	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere,
		BlueprintReadWrite,
		Meta = (ClampMin = 0, UIMax = 1))
	double TargetWeight;

public:
	/**  */
	UE_NODISCARD FORCEINLINE auto GetTargetWeight() const {
		return TargetWeight;
	}

public:
	/**
	 *
	 *
	 * @note Should be Interface but only descendants of an actor can be set to
	 * an interface variable in blueprints.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced)
	TObjectPtr<class UStaticTheaterActionBase> Action;

public:
	/**  */
	using FAction = decltype(Action);

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetAction() const noexcept {
		return Action;
	}

public:
	/**  */
	explicit FORCEINLINE FStaticTheaterWeightedAction(
		const FAction& Action = FAction(),
		const double TargetWeight = 1)
		: TargetWeight(TargetWeight), Action(Action) {}
};
