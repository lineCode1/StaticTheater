// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterMultiAction.h"

#include "StaticTheaterUniformRandomAction.generated.h"

/**
 *
 */
UCLASS(DisplayName = UniformRandom)
class STATICTHEATER_API UStaticTheaterUniformRandomAction
	: public UStaticTheaterMultiAction {

	GENERATED_BODY()

protected:
	void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const override;
};
