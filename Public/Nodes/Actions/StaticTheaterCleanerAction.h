// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterEmptyAction.h"

#include "StaticTheaterCleanerAction.generated.h"

/**
 *
 */
UCLASS(DisplayName = Cleaner)
class STATICTHEATER_API UStaticTheaterCleanerAction
	: public UStaticTheaterEmptyAction {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Action)
	uint8 bIsAllKeys: 1;

public:
	/**  */
	UE_NODISCARD FORCEINLINE bool IsAllKeys() const noexcept {
		return bIsAllKeys;
	}

public:
	/**
	 *
	 *
	 * @note Should be Interface but only descendants of an actor can be set
	 * to an interface variable in blueprints.
	 */
	UPROPERTY(EditAnywhere,
		BlueprintReadWrite,
		Category = Action,
		Meta = (EditConditionHides, EditCondition = "!bIsAllKeys"))
	TSet<TObjectPtr<class UStaticTheaterResourceKeyBase>> ResourceKeys;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetResourceKeys() const noexcept {
		return ResourceKeys;
	}

protected:
	void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const override;
};
