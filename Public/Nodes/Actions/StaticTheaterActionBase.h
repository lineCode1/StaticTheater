// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterActionInterface.h"

#include "StaticTheaterActionBase.generated.h"

/**
 *
 */
UCLASS(Abstract, Blueprintable, EditInlineNew, Meta = (ShowWorldContextPin))
class STATICTHEATER_API UStaticTheaterActionBase
	: public UObject
	, public IStaticTheaterActionInterface {

	GENERATED_BODY()

protected:
	void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const override {

		unimplemented();
	}
};
