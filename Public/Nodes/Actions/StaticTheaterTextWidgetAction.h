// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterEmptyAction.h"

#include "StaticTheaterTextWidgetAction.generated.h"

/**
 *
 */
UCLASS(DisplayName = TextWidget)
class STATICTHEATER_API UStaticTheaterTextWidgetAction
	: public UStaticTheaterEmptyAction {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Action)
	TObjectPtr<class UStaticTheaterTextWidgetResourceKey> ResourceKey;
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = Action)
	TArray<TObjectPtr<class UStaticTheaterTextWidgetActionValue>> ActionValues;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetResourceKey() const noexcept {
		return ResourceKey;
	}
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetActionValues() const noexcept {
		return ActionValues;
	}

protected:
	void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const override;
};
