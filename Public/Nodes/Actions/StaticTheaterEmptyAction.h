// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterActionBase.h"

#include "StaticTheaterEmptyAction.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterEmptyAction
	: public UStaticTheaterActionBase {

	GENERATED_BODY()

public:
	/**
	 *
	 *
	 * @note Should be Interface but only descendants of an actor can be set to
	 * an interface variable in blueprints.
	 */
	UPROPERTY(EditAnywhere,
		BlueprintReadWrite,
		Instanced,
		Category = Action,
		Meta = (DisplayPriority = 1))
	TObjectPtr<class UNodeSystemConditionBase> PlayCondition;
	/**
	 *
	 *
	 * @note Should be Interface but only descendants of an actor can be set to
	 * an interface variable in blueprints.
	 */
	UPROPERTY(EditAnywhere,
		BlueprintReadWrite,
		Instanced,
		Category = Action,
		Meta = (DisplayPriority = 1))
	TObjectPtr<class UNodeSystemOperationBase> PlayOperation;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetPlayCondition() const noexcept {
		return PlayCondition;
	}
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetPlayOperation() const noexcept {
		return PlayOperation;
	}

protected:
	/**
	 *
	 *
	 * @warning Check StateManager!
	 */
	UFUNCTION(BlueprintCallable,
		Category = "StaticTheater|Action",
		Meta = (
			// clang-format off
			Keywords =
				"6b6ae4 Static Theater Empty Action Try Play On State Manager Unsafe Is Executable",
			// clang-format on
			DisplayName = TryPlayOn_Unsafe))
	bool TryPlayOnStateManager_Unsafe(
		const TScriptInterface<class INodeSystemStateManagerInterface>&
			StateManager) const;
};
