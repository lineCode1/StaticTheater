// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterEmptyAction.h"
#include "StaticTheaterWeightedAction.h"

#include "StaticTheaterRandomAction.generated.h"

/**
 *
 */
UCLASS(DisplayName = Random)
class STATICTHEATER_API UStaticTheaterRandomAction
	: public UStaticTheaterEmptyAction {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Action)
	TArray<FStaticTheaterWeightedAction> WeightedActions;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetWeightedActions() const noexcept {
		return WeightedActions;
	}

protected:
	void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const override;
};
