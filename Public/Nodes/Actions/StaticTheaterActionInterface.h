// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <UObject/Interface.h>

#include "StaticTheaterActionInterface.generated.h"

class IStaticTheaterStateManagerInterface;

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UStaticTheaterActionInterface : public UInterface {
	GENERATED_BODY()
};

/**
 *
 */
class STATICTHEATER_API IStaticTheaterActionInterface {
	GENERATED_BODY()

public:
	/**
	 *
	 *
	 * @warning Check StateManager!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|Action",
		Meta = (
			// clang-format off
			Keywords =
				"6b6ae4 Static Theater Action Play On State Manager Unsafe Execute",
			// clang-format on
			DisplayName = PlayOn_Unsafe))
	void PlayOnStateManager_Unsafe(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const;

protected:
	virtual void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const = 0;
};
