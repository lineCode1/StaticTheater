// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterEmptyAction.h"

#include "StaticTheaterImageWidgetAction.generated.h"

/**
 *
 */
UCLASS(DisplayName = ImageWidget)
class STATICTHEATER_API UStaticTheaterImageWidgetAction
	: public UStaticTheaterEmptyAction {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Action)
	TObjectPtr<class UStaticTheaterImageWidgetResourceKey> ResourceKey;
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = Action)
	TArray<TObjectPtr<class UStaticTheaterImageWidgetActionValue>> ActionValues;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetResourceKey() const noexcept {
		return ResourceKey;
	}
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetActionValues() const noexcept {
		return ActionValues;
	}

protected:
	void PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const override;
};
