// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterEmptyAction.h"

#include "StaticTheaterMultiAction.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterMultiAction
	: public UStaticTheaterEmptyAction {

	GENERATED_BODY()

public:
	/**
	 *
	 *
	 * @note Should be Interface but only descendants of an actor can be set to
	 * an interface variable in blueprints.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = Action)
	TArray<TObjectPtr<UStaticTheaterActionBase>> Actions;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetActions() const noexcept {
		return Actions;
	}
};
