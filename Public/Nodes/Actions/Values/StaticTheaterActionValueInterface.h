// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <UObject/Interface.h>

#include "StaticTheaterActionValueInterface.generated.h"

class IStaticTheaterStageManagerInterface;

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UStaticTheaterActionValueInterface : public UInterface {
	GENERATED_BODY()
};

/**
 *
 */
class STATICTHEATER_API IStaticTheaterActionValueInterface {
	GENERATED_BODY()

public:
	/**
	 *
	 *
	 * @warning Check StageManager && Resource!
	 */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|ActionValue",
		Meta = (
			// clang-format off
			Keywords =
				"6b6ae4 Static Theater Action Value Apply On Stage Manager Unsafe",
			// clang-format on
			DisplayName = PlayOn_Unsafe))
	void PlayOnStageManager_Unsafe(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* Resource) const;

protected:
	virtual void PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const Resource) const = 0;
};
