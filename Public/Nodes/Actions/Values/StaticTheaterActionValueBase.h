// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterActionValueInterface.h"

#include "StaticTheaterActionValueBase.generated.h"

/**
 *
 */
UCLASS(Abstract, Blueprintable, EditInlineNew, Meta = (ShowWorldContextPin))
class STATICTHEATER_API UStaticTheaterActionValueBase
	: public UObject
	, public IStaticTheaterActionValueInterface {

	GENERATED_BODY()

protected:
	void PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const Resource) const override {

		unimplemented();
	}
};
