// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterTextWidgetActionValue.h"

#include "StaticTheaterUniformRandomTextStyleSetActionValue.generated.h"

/**
 *
 */
UCLASS(DisplayName = UniformRandomTextStyleSet)
class STATICTHEATER_API UStaticTheaterUniformRandomTextStyleSetActionValue
	: public UStaticTheaterTextWidgetActionValue {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere,
		BlueprintReadWrite,
		Category = ResourceData,
		Meta = (RequiredAssetDataTags =
					"RowStructure=/Script/UMG.RichTextStyleRow"))
	TArray<TObjectPtr<class UDataTable>> TextStyleSets;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetTextStyleSets() const noexcept {
		return TextStyleSets;
	}

protected:
	void PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const Resource) const override;
};
