// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterTextWidgetActionValue.h"

#include "StaticTheaterUniformRandomTextActionValue.generated.h"

/**
 *
 */
UCLASS(DisplayName = UniformRandomText)
class STATICTHEATER_API UStaticTheaterUniformRandomTextActionValue
	: public UStaticTheaterTextWidgetActionValue {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere,
		BlueprintReadWrite,
		Category = ActionValue,
		Meta = (MultiLine = true))
	TArray<FText> Texts;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetTexts() const noexcept {
		return Texts;
	}

protected:
	void PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const Resource) const override;
};
