// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterActionValueBase.h"

#include "StaticTheaterTextWidgetActionValue.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterTextWidgetActionValue
	: public UStaticTheaterActionValueBase {

	GENERATED_BODY()
};
