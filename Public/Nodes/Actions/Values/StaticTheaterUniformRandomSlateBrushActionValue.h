// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterImageWidgetActionValue.h"

#include "StaticTheaterUniformRandomSlateBrushActionValue.generated.h"

/**
 *
 */
UCLASS(DisplayName = UniformRandomSlateBrush)
class STATICTHEATER_API UStaticTheaterUniformRandomSlateBrushActionValue
	: public UStaticTheaterImageWidgetActionValue {

	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ActionValue)
	TArray<FSlateBrush> SlateBrushes;

public:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetSlateBrushes() const noexcept {
		return SlateBrushes;
	}

protected:
	void PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const Resource) const override;
};
