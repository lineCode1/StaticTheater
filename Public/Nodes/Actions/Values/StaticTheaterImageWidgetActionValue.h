// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "StaticTheaterActionValueBase.h"

#include "StaticTheaterImageWidgetActionValue.generated.h"

/**
 *
 */
UCLASS(Abstract)
class STATICTHEATER_API UStaticTheaterImageWidgetActionValue
	: public UStaticTheaterActionValueBase {

	GENERATED_BODY()
};
