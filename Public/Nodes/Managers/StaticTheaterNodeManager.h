// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include "States/Managers/StaticTheaterStateManagerInterface.h"
#include "States/Stages/Managers/StaticTheaterStageManagerInterface.h"

#include <Nodes/Managers/NodeSystemNodeManager.h>

#include "StaticTheaterNodeManager.generated.h"

/**
 * PlayerController NodeManager.
 *
 * TODO Add Within = PlayerController,
 */
UCLASS(ClassGroup = NodeManagers, Meta = (BlueprintSpawnableComponent))
class STATICTHEATER_API UStaticTheaterNodeManager
	: public UNodeSystemNodeManager
	, public IStaticTheaterStateManagerInterface
	, public IStaticTheaterStageManagerInterface {

	GENERATED_BODY()
	MModuleCoreGeneratedBodyUE(UStaticTheaterNodeManager);

protected:
	UE_NODISCARD TScriptInterface<IStaticTheaterStageManagerInterface>
	GetStageManager_Implementation() override {

		return this;
	}

protected:
	/**  */
	UPROPERTY(VisibleInstanceOnly, Category = NodeManager)
	TMap<const UObject*, UObject*> Resources;

protected:
	/**  */
	UE_NODISCARD FORCEINLINE const auto& GetResources() const noexcept {
		return Resources;
	}

protected:
	UE_NODISCARD UObject* GetResourceByResourceKey_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key)
		override;
	void RemoveResourceByResourceKey_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key)
		override;

	void EmptyResources_Implementation() override;

protected:
	/**  */
	UFUNCTION(BlueprintNativeEvent,
		BlueprintCallable,
		Category = "StaticTheater|NodeManager",
		Meta = (BlueprintProtected,
			Keywords = "6b6ae4 Static Theater Manager Play Actions"))
	void PlayActions();

protected:
	virtual void PlayActions_Implementation();

protected:
	void SetSourceSnapshot_Unsafe(UNodeSystemSnapshot* const Value) override;

	void FollowPath_Unsafe_Implementation(
		const TScriptInterface<INodeSystemPathInterface>& Value) override;

	void UndoState_Implementation() override;
};
