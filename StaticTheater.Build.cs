// Copyright 6b6ae4. All Rights Reserved.

using UnrealBuildTool;

public class StaticTheater : ModuleRules {
	public StaticTheater(ReadOnlyTargetRules Target) : base(Target) {
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core",
			"CoreUObject",
			"Engine",
			"SlateCore",
			"UMG",
			"MediaAssets",
			"ModuleCore",
			"NodeSystem" });

		if (Target.Type == TargetType.Editor) {
			PrivateDependencyModuleNames.AddRange(
				new string[] { "ModuleCoreEditor", "NodeSystemEditor" });
		}
	}
}
