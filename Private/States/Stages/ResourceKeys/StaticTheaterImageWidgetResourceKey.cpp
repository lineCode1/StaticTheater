// Copyright 6b6ae4. All Rights Reserved.

#include "States/Stages/ResourceKeys/StaticTheaterImageWidgetResourceKey.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "Widgets/StaticTheaterImageWidget.h"

#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>

UE_NODISCARD UObject* UStaticTheaterImageWidgetResourceKey::
	NewResourceForPlayerController_Unsafe_Implementation(
		APlayerController* const PlayerController) const {

	check(PlayerController);

	if (!MEnsureAlwaysLog(GetWidgetClass(), LogStaticTheater)) { return {}; }

	auto* const Resource =
		CreateWidget<UStaticTheaterImageWidget>(PlayerController,
			GetWidgetClass());
	check(Resource);
	Resource->AddToPlayerScreen(GetWidgetZOrder());

	return Resource;
}
