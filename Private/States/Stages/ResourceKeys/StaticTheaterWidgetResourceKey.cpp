// Copyright 6b6ae4. All Rights Reserved.

#include "States/Stages/ResourceKeys/StaticTheaterWidgetResourceKey.h"

#include "Blueprint/UserWidget.h"

void UStaticTheaterWidgetResourceKey::DeleteResource_Unsafe_Implementation(
	UObject* const Value) const {

	check(Value);

	auto* const Resource = Cast<UUserWidget>(Value);
	check(Resource);

	Resource->RemoveFromParent();
}
