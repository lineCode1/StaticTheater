// Copyright 6b6ae4. All Rights Reserved.

#include "States/Stages/ResourceKeys/StaticTheaterTextWidgetResourceKey.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "Widgets/StaticTheaterTextWidget.h"

#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>

UE_NODISCARD UObject* UStaticTheaterTextWidgetResourceKey::
	NewResourceForPlayerController_Unsafe_Implementation(
		APlayerController* const PlayerController) const {

	check(PlayerController);

	if (!MEnsureAlwaysLog(GetWidgetClass(), LogStaticTheater)) { return {}; }

	auto* const Resource =
		CreateWidget<UStaticTheaterTextWidget>(PlayerController,
			GetWidgetClass());
	check(Resource);
	Resource->AddToPlayerScreen(GetWidgetZOrder());

	return Resource;
}
