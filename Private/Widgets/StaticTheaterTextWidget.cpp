// Copyright 6b6ae4. All Rights Reserved.

#include "Widgets/StaticTheaterTextWidget.h"

#include <Components/RichTextBlock.h>

void UStaticTheaterTextWidget::SetText_Implementation(const FText& Value) {
	check(GetRichTextBlock());

	GetRichTextBlock()->SetText(Value);
}

void UStaticTheaterTextWidget::SetTextStyleSet_Unsafe_Implementation(
	UDataTable* const Value) {

	check(GetRichTextBlock() && Value);

	GetRichTextBlock()->SetTextStyleSet(Value);
}
