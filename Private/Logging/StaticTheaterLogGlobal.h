// Copyright 6b6ae4. All Rights Reserved.

#pragma once

#include <Logging/ModuleCoreLogMacrosGlobal.h>

DECLARE_LOG_CATEGORY_EXTERN(LogStaticTheater, Log, All);

/**
 *
 *
 * @param Verbosity [Error, Warning, Log..].
 * @param Format    Is optional.
 */
#define MLog(Verbosity, Format, ...)                                           \
	/* Should be ... and __VA_ARGS__. */                                       \
	/* Warning ", ##__VA_ARGS__" is not standard.. */                          \
	MLogCustom(LogStaticTheater, Verbosity, Format, ##__VA_ARGS__)
/**
 *
 *
 * @param Condition Expression.
 * @param Verbosity [Error, Warning, Log..].
 * @param Format    Is optional.
 */
#define MCLog(Condition, Verbosity, Format, ...)                               \
	/* Should be ... and __VA_ARGS__. */                                       \
	/* Warning ", ##__VA_ARGS__" is not standard.. */                          \
	MCLogCustom(Condition, LogStaticTheater, Verbosity, Format, ##__VA_ARGS__)
