// Copyright 6b6ae4. All Rights Reserved.

#include "Rules/Controllers/StaticTheaterPlayerController.h"

#include "Nodes/Managers/StaticTheaterNodeManager.h"

AStaticTheaterPlayerController::AStaticTheaterPlayerController(
	const FObjectInitializer& ObjectInitializer)
	: Super(
		  ObjectInitializer.SetDefaultSubobjectClass<UStaticTheaterNodeManager>(
			  Super::GetNodeManagerComponentName())) {}
