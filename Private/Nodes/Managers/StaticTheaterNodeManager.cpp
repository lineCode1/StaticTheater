// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Managers/StaticTheaterNodeManager.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "Nodes/Actions/StaticTheaterActionInterface.h"
#include "Nodes/StaticTheaterNode.h"
#include "States/Stages/ResourceKeys/StaticTheaterResourceKeyInterface.h"

#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>
#include <States/Snapshots/NodeSystemSnapshot.h>

UE_NODISCARD UObject*
UStaticTheaterNodeManager::GetResourceByResourceKey_Unsafe_Implementation(
	const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key) {

	check(Key);

	if (auto* const Resource = GetResources().Find(Key.GetObject())) {
		return *Resource;
	}

	auto* const PlayerController = Cast<APlayerController>(GetOwner());
	check(PlayerController);

	auto* const Resource = IStaticTheaterResourceKeyInterface::
		Execute_NewResourceForPlayerController_Unsafe(Key.GetObject(),
			PlayerController);
	Resources.Add(Key.GetObject(), Resource);

	return Resource;
}

void UStaticTheaterNodeManager::
	RemoveResourceByResourceKey_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterResourceKeyInterface>& Key) {

	check(Key);

	UObject* Resource = nullptr;
	if (!MEnsureAlwaysLog(
			Resources.RemoveAndCopyValue(Key.GetObject(), Resource),
			LogStaticTheater,
			Warning)) {

		return;
	}

	check(Resource);
	IStaticTheaterResourceKeyInterface::Execute_DeleteResource_Unsafe(
		Key.GetObject(),
		Resource);
}

void UStaticTheaterNodeManager::EmptyResources_Implementation() {
	for (const auto& ResourcePair : GetResources()) {
		check(ResourcePair.Key && ResourcePair.Value);
		IStaticTheaterResourceKeyInterface::Execute_DeleteResource_Unsafe(
			ResourcePair.Key,
			ResourcePair.Value);
	}

	Resources.Empty();
}

void UStaticTheaterNodeManager::PlayActions_Implementation() {
	check(GetSourceSnapshot());

	const auto& Nodes = GetSourceSnapshot()->GetNodes();
	if (!Nodes.IsEmpty()) {
		check(Nodes.Last());

		const auto* const Node =
			Cast<UStaticTheaterNode>(Nodes.Last().GetObject());
		if (!MEnsureAlwaysLog(Node, LogStaticTheater, Warning)) {
			LogSnapshots();
			return;
		}

		const TScriptInterface<IStaticTheaterStateManagerInterface>
			StateManager(this);
		check(StateManager);
		for (const auto& Action : Node->GetActions()) {
			if (!MEnsureAlwaysLog(Action, LogStaticTheater, Warning)) {
				continue;
			}

			IStaticTheaterActionInterface::Execute_PlayOnStateManager_Unsafe(
				Action.GetObject(),
				StateManager);
		}
	}

	LogSnapshots();
}

void UStaticTheaterNodeManager::SetSourceSnapshot_Unsafe(
	UNodeSystemSnapshot* const Value) {

	BaseType::SetSourceSnapshot_Unsafe(Value);

	PlayActions();
}

void UStaticTheaterNodeManager::FollowPath_Unsafe_Implementation(
	const TScriptInterface<INodeSystemPathInterface>& Value) {

	BaseType::FollowPath_Unsafe_Implementation(Value);

	PlayActions();
}

void UStaticTheaterNodeManager::UndoState_Implementation() {
	BaseType::UndoState_Implementation();

	PlayActions();
}
