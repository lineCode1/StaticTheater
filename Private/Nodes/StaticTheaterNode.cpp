// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/StaticTheaterNode.h"

#include "Nodes/Actions/StaticTheaterActionBase.h"

#if WITH_EDITORONLY_DATA
void UStaticTheaterNode::OnSetEditor_Actions() {
	Actions.Empty();
	for (const auto& Editor_Action : GetEditor_Actions()) {
		Actions.Emplace(Editor_Action.Get());
	}
}

void UStaticTheaterNode::PostEditChangeChainProperty(
	FPropertyChangedChainEvent& Value) {

	BaseType::PostEditChangeChainProperty(Value);

	if (Value.Property &&
		Value.Property->GetFName() ==
			GET_MEMBER_NAME_CHECKED(ThisType, Editor_Actions)) {

		OnSetEditor_Actions();
	}
}
#endif
