// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/StaticTheaterRandomAction.h"

#include "Logging/StaticTheaterLogGlobal.h"

#include <Libraries/NodeSystemFL.h>
#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>
#include <Math/ModuleCoreMath.h>

void UStaticTheaterRandomAction::PlayOnStateManager_Unsafe_Implementation(
	const TScriptInterface<IStaticTheaterStateManagerInterface>& StateManager)
	const {

	check(StateManager);
	const TScriptInterface<INodeSystemStateManagerInterface>
		NodeSystemStateManager(StateManager.GetObject());
	check(NodeSystemStateManager);

	if (!TryPlayOnStateManager_Unsafe(NodeSystemStateManager)) { return; }

	const auto* const WeightedAction =
		ModuleCore::Math::GetRandomWeighted(GetWeightedActions());
	if (!MEnsureAlwaysLogMsg(WeightedAction && WeightedAction->GetAction(),
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*NodeSystem::FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}

	IStaticTheaterActionInterface::Execute_PlayOnStateManager_Unsafe(
		WeightedAction->GetAction(),
		StateManager);
}
