// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/StaticTheaterCleanerAction.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "States/Managers/StaticTheaterStateManagerInterface.h"
#include "States/Stages/Managers/StaticTheaterStageManagerInterface.h"
#include "States/Stages/ResourceKeys/StaticTheaterResourceKeyBase.h"

#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>

void UStaticTheaterCleanerAction::PlayOnStateManager_Unsafe_Implementation(
	const TScriptInterface<IStaticTheaterStateManagerInterface>& StateManager)
	const {

	check(StateManager);
	const auto StageManager =
		IStaticTheaterStateManagerInterface::Execute_GetStageManager(
			StateManager.GetObject());
	check(StageManager);

	if (IsAllKeys()) {
		IStaticTheaterStageManagerInterface::Execute_EmptyResources(
			StageManager.GetObject());

		return;
	}

	for (const auto& ResourceKey : GetResourceKeys()) {
		if (!MEnsureAlwaysLog(ResourceKey, LogStaticTheater, Warning)) {
			continue;
		}

		IStaticTheaterStageManagerInterface::
			Execute_RemoveResourceByResourceKey_Unsafe(StageManager.GetObject(),
				ResourceKey);
	}
}
