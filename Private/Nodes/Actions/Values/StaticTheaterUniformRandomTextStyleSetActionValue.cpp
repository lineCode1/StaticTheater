// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/Values/StaticTheaterUniformRandomTextStyleSetActionValue.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "Widgets/StaticTheaterTextWidgetBase.h"

#include <Libraries/NodeSystemFL.h>
#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>
#include <Math/ModuleCoreMath.h>

using namespace NodeSystem;

void UStaticTheaterUniformRandomTextStyleSetActionValue::
	PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const NoCastResource) const {

	check(StageManager && NoCastResource);

	auto* const Resource = Cast<UStaticTheaterTextWidgetBase>(NoCastResource);
	check(Resource);

	if (!MEnsureAlwaysLogMsg(!GetTextStyleSets().IsEmpty(),
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}
	const auto& TextStyleSet =
		ModuleCore::Math::GetRandom_Unsafe(GetTextStyleSets());
	if (!MEnsureAlwaysLogMsg(TextStyleSet,
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}

	Resource->SetTextStyleSet_Unsafe(TextStyleSet);
}
