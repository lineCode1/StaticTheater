// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/Values/StaticTheaterUniformRandomTextActionValue.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "Widgets/StaticTheaterTextWidgetBase.h"

#include <Libraries/NodeSystemFL.h>
#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>
#include <Math/ModuleCoreMath.h>

using namespace NodeSystem;

void UStaticTheaterUniformRandomTextActionValue::
	PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const NoCastResource) const {

	check(StageManager && NoCastResource);

	auto* const Resource = Cast<UStaticTheaterTextWidgetBase>(NoCastResource);
	check(Resource);

	if (!MEnsureAlwaysLogMsg(!GetTexts().IsEmpty(),
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}

	Resource->SetText(ModuleCore::Math::GetRandom_Unsafe(GetTexts()));
}
