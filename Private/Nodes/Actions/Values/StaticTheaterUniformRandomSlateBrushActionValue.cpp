// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/Values/StaticTheaterUniformRandomSlateBrushActionValue.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "States/Stages/Managers/StaticTheaterStageManagerInterface.h"
#include "States/Stages/ResourceKeys/StaticTheaterImageWidgetResourceKey.h"
#include "Widgets/StaticTheaterImageWidget.h"

#include <Components/Image.h>
#include <Libraries/NodeSystemFL.h>
#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>
#include <Math/ModuleCoreMath.h>

using namespace NodeSystem;

void UStaticTheaterUniformRandomSlateBrushActionValue::
	PlayOnStageManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStageManagerInterface>&
			StageManager,
		UObject* const NoCastResource) const {

	check(StageManager && NoCastResource);

	auto* const Resource = Cast<UStaticTheaterImageWidget>(NoCastResource);
	check(Resource);

	if (!MEnsureAlwaysLogMsg(!GetSlateBrushes().IsEmpty(),
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}

	check(Resource->GetImage());
	Resource->GetImage()->SetBrush(
		ModuleCore::Math::GetRandom_Unsafe(GetSlateBrushes()));
}
