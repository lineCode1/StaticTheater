// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/StaticTheaterImageWidgetAction.h"

#include "Logging/StaticTheaterLogGlobal.h"
#include "Nodes/Actions/Values/StaticTheaterImageWidgetActionValue.h"
#include "States/Managers/StaticTheaterStateManagerInterface.h"
#include "States/Stages/Managers/StaticTheaterStageManagerInterface.h"

#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>

void UStaticTheaterImageWidgetAction::PlayOnStateManager_Unsafe_Implementation(
	const TScriptInterface<IStaticTheaterStateManagerInterface>& StateManager)
	const {

	check(StateManager);
	const TScriptInterface<INodeSystemStateManagerInterface>
		NodeSystemStateManager(StateManager.GetObject());
	check(NodeSystemStateManager);

	if (!TryPlayOnStateManager_Unsafe(NodeSystemStateManager)) { return; }

	const auto StageManager =
		IStaticTheaterStateManagerInterface::Execute_GetStageManager(
			StateManager.GetObject());
	check(StageManager);

	auto* const Resource = IStaticTheaterStageManagerInterface::
		Execute_GetResourceByResourceKey_Unsafe(StageManager.GetObject(),
			GetResourceKey());
	check(Resource);

	MCheckLog(GetActionValues().Num(), LogStaticTheater, Warning);
	for (const auto& ActionValue : GetActionValues()) {
		if (!MEnsureAlwaysLog(ActionValue, LogStaticTheater, Warning)) {
			continue;
		}

		IStaticTheaterActionValueInterface::Execute_PlayOnStageManager_Unsafe(
			ActionValue,
			StageManager,
			Resource);
	}
}
