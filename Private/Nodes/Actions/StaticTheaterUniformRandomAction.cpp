// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/StaticTheaterUniformRandomAction.h"

#include "Logging/StaticTheaterLogGlobal.h"

#include <Libraries/NodeSystemFL.h>
#include <Logging/ModuleCoreAssertionLogMacrosGlobal.h>
#include <Math/ModuleCoreMath.h>

using namespace NodeSystem;

void UStaticTheaterUniformRandomAction::
	PlayOnStateManager_Unsafe_Implementation(
		const TScriptInterface<IStaticTheaterStateManagerInterface>&
			StateManager) const {

	check(StateManager);
	const TScriptInterface<INodeSystemStateManagerInterface>
		NodeSystemStateManager(StateManager.GetObject());
	check(NodeSystemStateManager);

	if (!TryPlayOnStateManager_Unsafe(NodeSystemStateManager)) { return; }

	if (!MEnsureAlwaysLogMsg(!GetActions().IsEmpty(),
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}
	const auto& Operation = ModuleCore::Math::GetRandom_Unsafe(GetActions());
	if (!MEnsureAlwaysLogMsg(Operation,
			LogStaticTheater,
			Warning,
			TEXT("\nOuters = %s %s"),
			*FL::GetOutersAsString(this),
			*GetName())) {

		return;
	}

	IStaticTheaterActionInterface::Execute_PlayOnStateManager_Unsafe(Operation,
		StateManager);
}
