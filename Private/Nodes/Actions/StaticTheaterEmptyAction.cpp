// Copyright 6b6ae4. All Rights Reserved.

#include "Nodes/Actions/StaticTheaterEmptyAction.h"

#include "Nodes/Conditions/NodeSystemConditionBase.h"
#include "Nodes/Operations/NodeSystemOperationBase.h"
#include "States/Managers/NodeSystemStateManagerInterface.inl"

bool UStaticTheaterEmptyAction::TryPlayOnStateManager_Unsafe(
	const TScriptInterface<INodeSystemStateManagerInterface>& StateManager)
	const {

	check(StateManager);

	const bool bResult = !GetPlayCondition() ||
		INodeSystemConditionInterface::Execute_IsSatisfiedByStateManager_Unsafe(
			GetPlayCondition(),
			INodeSystemStateManagerInterface::ToConst(StateManager));

	if (bResult && GetPlayOperation()) {
		INodeSystemOperationInterface::Execute_ApplyOnStateManager_Unsafe(
			GetPlayOperation(),
			StateManager);
	}

	return bResult;
}
